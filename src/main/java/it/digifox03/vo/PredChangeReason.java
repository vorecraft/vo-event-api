package it.digifox03.vo;

public enum PredChangeReason {
    DIGEST,
    ESCAPE,
    RELEASE,
    ENTER,
    TRANSFER
}
