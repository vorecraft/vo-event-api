package it.digifox03.vo;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class PredChangeEvent extends Event {
    public final @NotNull Player prey;
    public final @Nullable Player oldPred;
    public final @Nullable Player newPred;
    public final @NotNull PredChangeReason reason;

    public PredChangeEvent(
            @NotNull Player prey, @Nullable Player oldPred, @Nullable Player newPred, @NotNull PredChangeReason reason
    ) {
        this.prey = prey;
        this.oldPred = oldPred;
        this.newPred = newPred;
        this.reason = reason;
    }
}
