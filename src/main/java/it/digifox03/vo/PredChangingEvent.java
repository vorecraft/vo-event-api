package it.digifox03.vo;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class PredChangingEvent extends PredChangeEvent {
    public PredChangingEvent(@NotNull Player prey, @Nullable Player oldPred, @Nullable Player newPred, @NotNull PredChangeReason reason) {
        super(prey, oldPred, newPred, reason);
    }

    static final HandlerList handlerList = new HandlerList();

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlerList;
    }

    @SuppressWarnings("unused")
    public static @NotNull HandlerList getHandlerList() {
        return handlerList;
    }
}

