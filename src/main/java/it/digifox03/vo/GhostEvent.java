package it.digifox03.vo;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class GhostEvent extends Event {
    public final @NotNull Player prey;
    public final @Nullable Player pred;

    public GhostEvent(@NotNull Player prey, @Nullable Player pred) {
        this.prey = prey;
        this.pred = pred;
    }

    static final HandlerList handlerList = new HandlerList();
    @Override
    public @NotNull HandlerList getHandlers() {
        return handlerList;
    }
    @SuppressWarnings("unused")
    public static @NotNull HandlerList getHandlerList() {
        return handlerList;
    }
}
