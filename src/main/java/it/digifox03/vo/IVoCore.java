package it.digifox03.vo;

import java.util.Collection;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IVoCore {
    @Nullable Player pred(@NotNull Player prey);
    @NotNull Collection<Player> preys(@NotNull Player pred);
    @Nullable Player predGhost(@NotNull Player prey);
    @NotNull Collection<Player> preyGhosts(@NotNull Player pred);

    void digest(@NotNull Player prey);
    void stop(@NotNull Player prey);
    void release(@NotNull Player prey);
    void enter(@NotNull Player prey, @NotNull Player pred);
    void transfer(@NotNull Player prey, @NotNull Player pred);
    void setGhost(@NotNull Player prey, @Nullable Player pred);
}
