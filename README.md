# Vo event api
License :: MIT

Subscribe to the events using spigot's eventhandler system!

Call the core with getServer().getServicesManager().getRegistration(IVoCore.class)

Whenever the pred of a player prey is changed; the following will happen:

1. A PredChangingEvent is fired.
2. The effect is visible from IVoCore
3. A PredChangedEvent is fired.

The parameters for both events are the same, and will be the following:
 
 prey: The player whose pred is being changed.
 
 oldPred: The former pred of the player.
 
 newPred: The new pred of the player.

The following must always be true:
 
 1. During and before the PredChangingEvent, the pred of prey is oldPred.
  2. During and after the PredChangingEvent, the pred of prey is newPred.

The both events also take an additional parameter reason;
the value of that parameter must be one of the following:
  DIGEST, ESCAPE, RELEASE, ENTER, and TRANSFER

DIGEST is used when the pred of the player is set to null.

RELEASE or ESCAPE is used when the pred of the player is set to the pred of the player's pred.

ENTER is used when the pred of the player is set to another player,
and both the player and the new pred share the same pred.

TRANSFER is used when the pred of the player is set to another player,
and both the old pred and the new pred share the same pred.